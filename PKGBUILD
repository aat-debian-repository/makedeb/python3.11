# Maintainer: Antoni Aloy Torrens <antoni.aloytorrens@gmail.com>
pkgname=python3.11
pkgver=3.11.0
pkgrel=0
pkgdesc="Python Interpreter"
arch=('amd64' 'i386' 'arm64' 'armhf' 'armel')
LICENSE=('PSF-2.0')
makedepends=('wget' 'build-essential' 'libncursesw5-dev' 'libssl-dev' 'libsqlite3-dev' 'tk-dev' 'libgdbm-dev' 'libc6-dev' 'libbz2-dev' 'libffi-dev' 'zlib1g-dev' 'libmpdec-dev')
depends=('wget' 'libncursesw5' 'libssl1.1' 'sqlite3' 'tk' 'libgdbm6' 'libc6' 'bzip2' 'libffi7' 'zlib1g' 'libmpdec3')
url="https://www.python.org"
source=("https://www.python.org/ftp/python/${pkgver}/Python-${pkgver}.tgz")
sha512sums=('559318b2c180e1060cc80cf0ff3b2df1d33b8f08bcdf3cd44f72a8eb4c759ca8488c0f65d8808d4f539411e6ed893aaf22bcacf249aff3c55f629df7e19b9343')

build() {
        # Necessary variables (otherwise arm architectures will complain about `C compiler cannot create executables`)
        export DEB_BUILD_GNU_TYPE=$(cut -d "=" -f2 <<< $(dpkg-architecture | grep DEB_BUILD_GNU_TYPE))
        export DEB_HOST_GNU_TYPE=$(cut -d "=" -f2 <<< $(dpkg-architecture | grep DEB_HOST_GNU_TYPE))
        export DEB_HOST_MULTIARCH=$(cut -d "=" -f2 <<< $(dpkg-architecture | grep DEB_HOST_MULTIARCH))

        # C compiler will not create executables due to -march and -mtune is detected as x86_64 (as we are running in chroot)
        # Override this and remove march and mtune

        # Also, in ARM architectures: cc1: error: '-fcf-protection=full' is not supported for this target (see config.log file)
        # We need to remove this CFLAG
        if [[ $DEB_HOST_GNU_TYPE == *"aarch64"* ]] || [[ $DEB_HOST_GNU_TYPE == *"armhf"* ]] || [[ $DEB_HOST_GNU_TYPE == *"armel"* ]]; then
                export CFLAGS='-O2 -pipe -fno-plt -fexceptions -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -fstack-clash-protection'
        else
                if [[ $DEB_HOST_GNU_TYPE == *"x86_64"* ]]; then
                        export CFLAGS='-march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -fstack-clash-protection -fcf-protection'
                else
                        export CFLAGS='-O2 -pipe -fno-plt -fexceptions -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -fstack-clash-protection -fcf-protection'
                fi
        fi

        cd Python-${pkgver}
        ./configure \
                --build=${DEB_BUILD_GNU_TYPE} \
                --host=${DEB_HOST_GNU_TYPE} \
                --libdir=/usr/lib \
                --prefix=/usr \
                --enable-ipv6 \
                --enable-loadable-sqlite-extensions \
                --enable-shared \
                --enable-optimizations \
                --with-computed-gotos \
                --with-dbmliborder=gdbm:ndbm \
                --with-system-expat \
                --with-system-ffi \
                --with-system-libmpdec \
                --without-ensurepip                  
        make
}

package() {
        cd Python-${pkgver}
        msg2 "Setting up data..."
        make DESTDIR=${pkgdir} altinstall

        # Remove unneeded library
        rm -rf ${pkgdir}/usr/lib/libpython3.so
}
